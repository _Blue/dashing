#My dashboard

This repository contains a web based, django powered dashboard.

The project is based on [django-dashing](https://github.com/talpor/django-dashing), and is using some widgets from [widget-party](https://github.com/mverteuil/widget-party)


#Configure

To add widgets, fill the `widget` variables in `blue_dashing/widgets.py`.

Example configuration:

```
ping_pattern = ['blue', 'Blue', 'channel', 'everyone'] # Slack highlights

widgets = [
            {
              'class': Mining, # Displays how fast your machine is mining ethereum
              'title': 'Mining',
              'args':
                {'address':'eth_address'}
            },
            {
              'class': CryptoCurrencies, # Displays ethereum and bitcoin value
              'title': 'CryptoCurrencies'
            },
            {
              'class': HTTPChecker, # Verifies that a website is responding
              'title': 'bluecode.fr',
              'args':
                {'uri': 'http://bluecode.fr'}
            },
            {
              'class': SMTPChecker, # Verifies that an SMTP server is responding
              'title': 'mail.bluecode.fr',
              'args':
                {'address': 'mail.bluecode.fr', 'port': 25}
            },
            {
              'class': IcingaChecker, # Displays Icinga monitoring status
              'title': 'Icinga',
              'args':
              {'address': 'https://my.icinga.server', 'auth': ('user', 'pass')}
            },
            {
              'class': FacebookWidget, # Displays Facebook unread message count
              'title': 'Facebook',
              'args':
              {'cookie': 'my_facebook_cookie'
            },
            {
              'class': SlackWidget, # Displays slack unread messages and highlights
              'title': 'My slack team',
              'args':
              {
                'uri': 'https://my.slack.com',
                'token': '',
                'ping_pattern': ping_pattern
              }
            },
            {
              'class': SlackStream, # Displays slack messages in real time
              'title': 'SlackStream'
            }
          ]
```

You also need to add widgets in `blue_dashing/dashing-config.js`:

Example:
```
var dashboard = new Dashboard();

# Displays ethereum and bitcoin values
dashboard.addWidget('CryptoCurrencies', 'Number', {
    getData: function () {
        var self = this;
        Dashing.utils.get('CryptoCurrencies', function(scope) {
            $.extend(self.scope, scope);
        });
    },
    interval: 5000
});

# Displays mining speed
dashboard.addWidget('Mining', 'Number', {
    getData: function () {
        var self = this;
        Dashing.utils.get('Mining', function(scope) {
            $.extend(self.scope, scope);
        });
    },
    interval: 5000
});

# Displays slack messages in real time
dashboard.addWidget('SlackStream', 'List', {
    getData: function () {
        var self = this;
        Dashing.utils.get('SlackStream', function(scope) {
            $.extend(self.scope, scope);
        });
    },
});

# Displays the weather forecast
dashboard.addWidget('weather_widget', 'Weather', {
    WOEID: 615702
});

# Displays website status
dashboard.addWidget('bluecode_fr', 'BuildStatus', {
     getData: function () {
        var self = this;
        Dashing.utils.get('bluecode.fr', function(scope) {
            $.extend(self.scope, scope);
        });
    },
interval: 5000
});

# Displays Icinga monitoring status
dashboard.addWidget('Icinga', 'BuildStatus', {
     getData: function () {
        var self = this;
        Dashing.utils.get('Icinga', function(scope) {
            $.extend(self.scope, scope);
        });

    },
interval: 5000
});

# Displays unread Facebook messages
dashboard.addWidget('Facebook', 'BuildStatus', {
     getData: function () {
        var self = this;
        Dashing.utils.get('Facebook', function(scope) {
            $.extend(self.scope, scope);
        });

    },
interval: 5000
});
```

#Deploy

Install the requirements:

```
pip install -r requirements.txt
```

If you need the Facebook widget, you'll need to install [wkhtmltopdf](http://wkhtmltopdf.org).  
To get a Facebook cookie, you can login using Firefox and get it through the developer console. 


Create a server secret in `blue_dashing/settings.py`

To start a development server, run:

```
./manage.py runserver
```


#Screenshot

![Screenshot](images/screenshot.png)
