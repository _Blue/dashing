import time
import sys
import os
import traceback
from .settings import TIME_ZONE, TIME_FORMAT
from dashing.widgets import NumberWidget, Widget, ListWidget

os.environ['TZ'] = TIME_ZONE

def format_time(ts):
    return time.strftime(TIME_FORMAT, time.localtime(ts))


class RefreshingWidget():
    instance = None
    last_refresh = 0
    timeout = 30
    detail = None
    text= ''
    more_infos = ''

    def __new__(cls): # Make sure we won't duplicate those
        if cls.instance is None:
            cls.instance = object.__new__(cls)
        return cls.instance

    def invalidate(self):
        self.last_refresh = 0

    def refresh(self):
        now = time.time()

        if now > self.last_refresh + self.timeout:
            self.last_refresh = now

            try:
                self.detail  = ''
                self.text = ''
                self.more_infos = ''
                self.status = self.update()
            except Exception as e:
                traceback.print_exc(file=sys.stderr)
                self.status = 'failure'
                self.text = 'ERROR!'
                self.detail = traceback.format_exc()

    def get_title(self):
        return self.title

    def get_updated_at(self):
        self.refresh()
        return format_time(self.last_refresh)

    def get_detail(self):
        self.refresh()
        return self.detail

    def get_more_info(self):
        self.refresh()
        return self.more_infos

    def get_value(self):
        self.refresh()
        return self.text


class StatusWidget(RefreshingWidget, Widget):
    status = 'progress'
    updated_at = 0
    detail = ''

    def get_status(self):
        return self.status

    def get_context(self):
        return {
            'title': self.get_title(),
            'updatedAt': self.get_updated_at(),
            'detail': self.get_detail(),
            'moreInfo': self.get_more_info(),
            'buildNumber': self.get_value(),
            'buildStatus': self.get_status(),
        }

class InformedStatusWidget(RefreshingWidget, Widget):
    status = 'progress'
    value = ''
    detail = ''
    updated_at = 0
    extra = []

    def get_context(self):
        self.refresh()

        return {
                'title': self.get_title(),
                'updatedAt': self.get_updated_at(),
                'extra': self.extra if not self.detail else '',
                'value': self.detail if self.detail else self.value,
                'informedNumber': self.status
                }

class GraphWidget(RefreshingWidget, Widget):

    def get_context(self):
        self.refresh()

        return {
                'title': self.get_title(),
                'moreInfo': self.get_more_info(),
                'value': self.get_value(),
                'detail': self.get_detail(),
                'data': self.get_data(),
               }
