import requests
import socket
import sys
import re
import json

from .base_widgets import StatusWidget, InformedStatusWidget
from requests.auth import HTTPBasicAuth


SMTP_HELO = 'helo server\n'.encode('UTF-8')
SMTP_QUIT = 'quit\n'.encode('UTF-8')
SMTP_EXPECTED = '220.*\r\n250.*\r\n'

class HTTPChecker(StatusWidget):
    def update(self):
        self.detail = '' # Clear any error status
        self.text = '' # Clear any error status
        if requests.get(self.uri).status_code == 200:
            return 'success'
        return 'failure'

class SMTPChecker(StatusWidget):
    title = None
    address = None
    port = None

    def update(self):

        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((self.address, self.port))
        s.send(SMTP_HELO)

        response = s.recv(1).decode('UTF-8') # Not very elegant, but let's keep it simple
        while response and response.count('\r\n') < 2:
            response += s.recv(1).decode('UTF-8')

        s.send(SMTP_QUIT)
        s.close()
        if re.match(SMTP_EXPECTED, response):
            self.detail = '' # Clear any error status
            self.text = '' # Clear any error status

            return 'success'
        else:
            print('SMTP: expected: %s, got %s'
                % (SMTP_EXPECTED, response),
                file=sys.stderr)


class IcingaChecker(StatusWidget):
    title = None
    address = None
    auth = None

    def update(self):
        criticals = 0
        ack = 0
        warnings = 0

        req = requests.get(self.address
            + '/v1/objects/services?filter=service.state!=ServiceOK',
            auth=self.auth)

        assert req.status_code == 200
        for e in req.json()["results"]:

            if e['attrs']['acknowledgement'] == 1:
                ack = ack + 1
            elif e['attrs']['state'] == 2:
                criticals = criticals + 1
            elif e['attrs']['state'] == 1:
                warnings = warnings + 1

        self.text = '☠: %s' % criticals
        self.detail = '⚠: %s' % warnings
        self.more_infos = '✓: %s' % ack


        if (criticals != 0):
            return 'failure'

        return 'unstable' if warnings != 0 else 'success'

class HApiChecker(InformedStatusWidget):
    uri = None
    settings = None
    request_timeout = 15

    def update(self):
        self.extra = []
        self.detail = ''
        self.text = ''

        result = requests.post(self.uri,
                timeout=self.request_timeout,
                json=self.settings)
        if result.status_code != 200:
            self.value = 'HTTP request returned: ' + str(result.status_code)
            return 'failure'
        else:
            self.value = ''

        services = result.json()
        for e in services:
            self.extra.append({'label': e['name'], 'value': e['message']})

        value_map = [
                     ('CRITICAL', 'failure'),
                     ('UNKNOWN', 'progress'),
                     ('WARNING', 'unstable')
                    ]

        for mapping in value_map:
            if any(e['state'] == mapping[0] for e in services):
                return mapping[1]

        assert all(e['state'] == 'OK' for e in services)

        return 'success'

class XocialChecker(StatusWidget):
    timeout = 300

    endpoint = None

    def update(self):

        response = requests.get(self.endpoint + '/source')
        response.raise_for_status()

        body = response.json()
        errors = sum(len(e['errors']) for e in body)

        self.more_infos = "{} errors".format(errors)

        if (all(e['status'] == 'Running' for e in body)):
            self.text = "Healthy"
            self.detail = "Sources: " + ", ".join([e['display_name'] for e in body])
            return 'unstable' if errors else 'success'
        else:
            self.text = "Error"
            self.detail = "Sources status: " + ", ".join([e['status'] for e in body])
            return 'failure'
