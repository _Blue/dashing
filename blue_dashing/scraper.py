from .base_widgets import InformedStatusWidget
import time
import random
import psutil

from collections import OrderedDict
from selenium import webdriver
from selenium.webdriver.firefox.options import Options, DesiredCapabilities
from selenium.webdriver.common.keys import Keys

class ScraperWidget(InformedStatusWidget):
    prepare_routine = None
    extract_routine = None
    uri = None
    timeout = 60 * 60 * 6

    def update(self):

        options = Options()
        options.add_argument('-private')
        options.headless = True

        profile = webdriver.FirefoxProfile()
        profile.set_preference("browser.cache.disk.enable", False)
        profile.set_preference("browser.cache.memory.enable", False)
        profile.set_preference("browser.cache.offline.enable", False)
        profile.set_preference("network.http.use-cache", False)
        profile.set_preference("browser.privatebrowsing.autostart", True)
        profile.set_preference("dom.disable_beforeunload", True) # Some web pages ask for confirm beforequitting
        profile.set_preference("browser.tabs.warnOnClose", False)
        profile.set_preference("network.proxy.type", 1)
        profile.set_preference("dom.webdriver.enabled", False)
        profile.set_preference('useAutomationExtension', False)
        profile.update_preferences()

        driver = webdriver.Firefox(profile, options=options, desired_capabilities=DesiredCapabilities.FIREFOX)
        try:
            driver.delete_all_cookies()
            driver.get(self.uri)
            if self.__class__.prepare_routine:
                self.__class__.prepare_routine(driver)

            self.extra = []
            self.detail = ''

            content = self.__class__.extract_routine(driver)

            self.extra = [{'label': e, 'value': content[e]} for e in content]

            return 'success'
        finally:
            # Huge hack around geckodriver bug causing driver.quit() to block forever
            # This is really bad because it creates a race between different instances of this widget
            children = [p for p in psutil.process_iter() if p.name() == 'geckodriver' or p.name() == 'firefox-esr']
            for e in children:
                e.kill()

            driver.quit()
