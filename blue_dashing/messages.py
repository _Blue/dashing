from dashing.widgets import ListWidget
from threading import Lock


class MessagesWidget(ListWidget):
    timeout = 10
    max_items = 20
    messages = []
    errors = []
    mutex = Lock()
    instance = None
    message_tiles = {}

    def __new__(cls):
        if cls.instance is None:
            cls.instance = object.__new__(cls)

        return cls.instance

    def register_tile(self, url: str, tile):
        if url in self.message_tiles:
            assert self.message_tiles[url] is tile
        else:
            self.message_tiles[url] = tile

    def invalidate_tile(self, url: str):
        if url in self.message_tiles:
            print('Invalidating widget: ' + url)
            self.message_tiles[url].invalidate()
        else:
            print('No matching tile to be invalidated: ' + url)

    def add_message(self, author: str, time: str, content: str, url: str):
        self.mutex.acquire()

        try:
            message = {
                        'url': url,
                        'author': time,
                        'sha': author,
                        'message': content
                      }

            self.messages.append(message)

            self.messages = self.messages[-self.max_items:]
        finally:
            self.mutex.release()

    def remove_error_state(self, uri: str):
        self.mutex.acquire()

        try:
            self.errors = [e for e in self.errors if e['uri'] != uri]
        finally:
            self.mutex.release()


    def add_error(self, time: str, content: str, uri: str):
        self.mutex.acquire()

        try:
            self.errors.append({
                                'author': time,
                                'sha': 'error',
                                'message': content,
                                'uri': uri
                               })
        finally:
            self.mutex.release()

    def get_data(self):
        self.mutex.acquire()

        try:
            return self.errors if self.errors else self.messages
        finally:
            self.mutex.release()
