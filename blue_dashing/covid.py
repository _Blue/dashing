import requests
import backoff
import json
import time
import csv
from io import StringIO
from requests.auth import HTTPBasicAuth
from .base_widgets import GraphWidget, TIME_FORMAT
from datetime import datetime, timedelta

MAX_HISTORY_DAYS = 90
CSV_DATE_FORMAT = '%Y-%m-%d'


def format_number(value) -> str:
    value = float(value)
    if value < 1000:
        return str(value)
    elif value < 1000000:
        return str(round(value / 1000, 2)) + 'K'
    else:
        return str(round(value / 1000000, 2)) + 'M'


class CovidWidget(GraphWidget):
    country = None # Country name
    timeout = 3600 # Refresh timeout, in seconds
    current_value = None # Holds today's value
    detail = None
    historical_values = None
    last_updated = None

    def get_moving_average(self, values: list):
        assert len(values) >= 7

        result = []
        for i in range(0, len(values) - 7):
            window = values[i : i + 7]
            result.append(round(sum(window) / 7))

        return result


    def get_value(self):
        self.refresh()

        return self.current_value

    def get_detail(self):
        return self.detail

    def get_data(self):
        self.refresh()

        def make_row(position, value) -> dict:
            return {'x': position, 'y': value}

        return [make_row(e[0], e[1]) for e in enumerate(self.historical_values)]

    def get_more_info(self):
        return self.last_updated


class CovidCasesWidget(CovidWidget):
    def update(self):
        response = requests.get('https://raw.githubusercontent.com/owid/covid-19-data/master/public/data/jhu/full_data.csv')
        response.raise_for_status()

        rows = [e for e in csv.reader(StringIO(response.content.decode())) if e[1] == self.country]
        if not rows:
            raise RuntimeError("Not data found for country: " + self.country)


        # CSV format: date,location,new_cases,new_deaths,total_cases,total_deaths,weekly_cases,weekly_deaths,biweekly_cases,biweekly_deaths
        latest = sorted(rows, key = lambda e: e[0])[-1]

        # Parse dates and keep number of cases
        cases = [(datetime.strptime(e[0], CSV_DATE_FORMAT), int(float(e[2])) if e[2] else 0) for e in rows]

        # Sort by date
        sorted_cases = sorted(cases, key = lambda e: e[0])

        # Validate that no day is missing (would mess up the weekly average)
        for i, e in enumerate(sorted_cases):
            if i == 0:
                continue

            if (sorted_cases[i][0] - sorted_cases[i - 1][0]).days != 1:
                raise RuntimeError("Missing day between " + str(sorted_cases[i - 1]) + " and " + str(sorted_cases[i]))

        increase = format_number(sorted_cases[-1][1])
        self.detail = f'+{increase}'
        values = [int(e[1]) if e else 0 for e in sorted_cases][-MAX_HISTORY_DAYS:]
        self.historical_values = self.get_moving_average(values)
        self.last_updated = sorted_cases[-1][0].strftime(TIME_FORMAT)
        self.current_value = format_number(sum(e[1] for e in sorted_cases))


class CovidVaccinationsWidget(CovidWidget):
    def update(self):
        response = requests.get(f'https://raw.githubusercontent.com/owid/covid-19-data/master/public/data/vaccinations/country_data/{self.country}.csv')
        response.raise_for_status()

        content = [e for e in csv.reader(StringIO(response.content.decode()))]
        columns = content[0]
        date_column = columns.index('date')
        value_column = columns.index('people_vaccinated')

        rows = content[1:]
        if not rows:
            raise RuntimeError("Not data found for country: " + self.country)

        # Parse dates and keep number of cases
        entries = [(datetime.strptime(e[date_column], CSV_DATE_FORMAT), int(float(e[value_column])) if e[value_column] else 0) for e in rows]

        # Sort by date
        sorted_entries = sorted(entries, key = lambda e: e[0])

        # Generate vaccination counts
        first_date = sorted_entries[0][0]
        last_date = sorted_entries[-1][0]

        values = []
        for day in [first_date + timedelta(days=i) for i in range((last_date - first_date).days + 1)]:
            entry = next((e[1] for e in sorted_entries if e[0] == day), None)

            values.append(float(entry) if entry is not None else values[-1])

        increase = format_number(values[-1] - values[-2])
        self.detail = f'+{increase}'
        self.historical_values = self.get_moving_average(values[-MAX_HISTORY_DAYS:])
        self.last_updated = sorted_entries[-1][0].strftime(TIME_FORMAT)
        self.current_value = format_number(values[-1])

class CovidUsStateVaccinationsWidget(CovidWidget):
    state = None

    def update(self):
        response = requests.get(f'https://raw.githubusercontent.com/owid/covid-19-data/master/public/data/vaccinations/us_state_vaccinations.csv')
        response.raise_for_status()

        rows = [e for e in csv.reader(StringIO(response.content.decode())) if e[1] == self.state]
        if not rows:
            raise RuntimeError("Not data found for state: " + self.state)

        # date,location,total_vaccinations,total_distributed,people_vaccinated,people_fully_vaccinated_per_hundred,total_vaccinations_per_hundred,people_fully_vaccinated,people_vaccinated_per_hundred,distributed_per_hundred,daily_vaccinations_raw,daily_vaccinations,daily_vaccinations_per_million,share_doses_used
        latest = sorted(rows, key = lambda e: e[1])[-1]

        # Parse dates and keep number of cases
        entries = [(datetime.strptime(e[0], CSV_DATE_FORMAT), float(e[8]) if e[8] else 0) for e in rows]

        # Sort by date
        sorted_entries = sorted(entries, key = lambda e: e[0])

        # Generate vaccination counts
        first_date = sorted_entries[0][0]
        last_date = sorted_entries[-1][0]

        values = []
        for day in [first_date + timedelta(days=i) for i in range((last_date - first_date).days + 1)]:
            entry = next((e[1] for e in sorted_entries if e[0] == day), None)

            values.append(float(entry) if not values or entry else values[-1])

        increase = str(round(values[-1] - values[-2], 2))
        self.detail = f'+{increase}%'
        self.historical_values = values[-MAX_HISTORY_DAYS:]
        self.last_updated = sorted_entries[-1][0].strftime(TIME_FORMAT)
        self.current_value = str(round(values[-1], 2)) + '%'

