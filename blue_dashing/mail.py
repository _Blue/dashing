from .base_widgets import RefreshingWidget
from dashing.widgets import ListWidget
import imaplib
import email
from email.header import decode_header, make_header
import base64
from multiprocessing import Lock
import traceback
import sys
import msal
import os

MAX_SUBJECT = 66
MAX_ADDRESS = 40

class MailChecker:
    client = None
    user = None
    password = None
    client_id = None
    authority = None
    token_cache_path = None
    scope = None

    def __init__(self, server: str, user: str, password = None, client_id = None, authority = None, token_cache_path = None, scope = None):
        self.server = server
        self.user = user
        self.password = password
        self.client_id = client_id
        self.authority = authority
        self.token_cache_path = token_cache_path
        self.scope = scope


    def init(self):
        self.client = imaplib.IMAP4_SSL(self.server)

        if self.password is not None:
            self.client.login(self.user, base64.b64decode(self.password).decode())
        else:
            cache = msal.SerializableTokenCache()
            if not os.path.exists(self.token_cache_path):
                raise RuntimeError(f'Token cache not found: {self.token_cache_path}')

            with open(self.token_cache_path) as fd:
                cache.deserialize(fd.read())

            app = msal.PublicClientApplication(client_id=self.client_id, authority=self.authority, token_cache=cache)
            account = next((e for e in app.get_accounts() if e['username'] == self.user), None)

            if account is None:
                raise RuntimeError(f'Account "{self.user}" not found in token cache: {self.token_cache_path}')

            result = app.acquire_token_silent([self.scope], account=account)
            token = result.get('access_token') if result else None
            if token is None:
                raise RuntimeError(f'Failed to acquire silent token for account "{self.user}". Result: {result}')

            if cache.has_state_changed:
                with open(self.token_cache_path, 'w') as fd:
                    fd.write(cache.serialize())

                print(f'Updated cached token in {self.token_cache_path}', file=sys.stderr)

            def generate_auth_string(user, token):
                auth_string = f"user={user}\1auth=Bearer {token}\1\1"
                return auth_string

            self.client.authenticate('XOAUTH2', lambda x: generate_auth_string(self.user, token))


    def close(self):
        try:
            self.client.close()
            self.client.logout()
        except imaplib.IMAP4.error:
            pass

        self.client = None

    def get_new_emails(self) -> list:
        self.client.select(readonly=True)
        (result, ids) = self.client.search(None, '(UNSEEN)')
        assert result == 'OK'

        def make_email(id: str):
            (result, content) = self.client.fetch(id, '(RFC822)')
            assert result == 'OK'

            return email.message_from_bytes(content[0][1])

        return [make_email(e) for e in ids[0].decode().split(' ') if e]

    def make_list(input: list, max_line: int, max_size: int) -> str:
        return ', '.join([e[:max_line:] if e else '[Invalid]' for e in input][:max_size:])

    def get_unread_emails(self):
        if not self.client:
            self.init()

        try:
            return self.get_new_emails()
        except (imaplib.IMAP4.abort, imaplib.IMAP4.error): # IMAP connection timed out
            self.close()
            self.init() # Create fresh connection
            return self.get_new_emails()

class MailWidget(ListWidget, RefreshingWidget):
    addresses = None
    settings = None
    status = 'progress'
    mutex = Lock()
    data = []
    errors = []
    timeout = 500

    def make_row(self, message, address: str):
        return {
                'url': '',
                'sha': str(make_header(decode_header(message['Subject'])))[:MAX_SUBJECT:],
                'author': address,
                'message': str(make_header(decode_header(message['From'])))[:MAX_ADDRESS:]
                }

    def make_error_row(self, exception, address: str):
        return {
                'url': '',
                'sha': 'ERROR',
                'author': address,
                'message': str(exception)
                }


    def update_impl(self):
        if not self.addresses:
            self.addresses = [MailChecker(**e) for e in self.settings]

        mails = []
        errors = []
        for e in self.addresses:
            try:
                mails += [self.make_row(mail, e.user) for mail in e.get_unread_emails()]
            except Exception as error:
                print(f'Error in mail module: {traceback.format_exc()}', file=sys.stderr)
                errors += [self.make_error_row(error, e.user)]

        self.data = errors + mails

        if errors:
            return 'progress'

        return 'failure' if mails else 'success'

    def update(self):
        with self.mutex:
            return self.update_impl()

    def get_context(self):
        self.refresh()

        return {
                'title': self.get_title(),
                'updatedAt': self.get_updated_at(),
                'commitList': self.status,
                'data': self.data
               }
