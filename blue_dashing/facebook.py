import time
from .base_widgets import StatusWidget
from fbchat import Client
from fbchat.models import Group
from traceback import format_exc
from threading import Thread
from .messages import MessagesWidget
from .base_widgets import format_time

class FacebookClient(Client):
    widget = MessagesWidget()

    def __init__(self, *args, **kwargs):
        super().__init__(self, *args, **kwargs)

        self.setActiveStatus(False)

    def onMessage(self, author_id, message, message_object, thread_id, thread_type, **kwargs):
        try:
            user = self.fetchUserInfo(author_id)

            self.widget.add_message(user[author_id].name, format_time(time.time()), message, 'https://facebook.com')
            self.widget.invalidate_tile("https://facebook.com")
        except Exception as e:
            print(format_exc(e))
            self.widget.add_error(format_time(time.time()), format_exc(e), 'https://facebook.com')

    def listenForever(self):
        thread = Thread(target=FacebookClient.listen, args=(self,))
        thread.daemon = True
        thread.start()


# Widget that displays unread facebook discussions
# To get a cookie:
# >>> from fbchat import Client
# >>> client = Client('username','password');
# >>> client.getSession()
class FacebookWidget(StatusWidget):
    cookie = None
    client = None
    username = None
    password = None
    timeout = 300

    def __init__(self):
        MessagesWidget().register_tile("https://facebook.com", self)

    def get_messages(self):

        if not self.client: # Initialize client on first call
            self.client = FacebookClient(self.username, self.password, session_cookies=self.cookie)
            self.client.listenForever()

        threads = set(self.client.fetchUnread() + self.client.fetchUnseen())

        def stringify_user(user):
            return user.first_name or '' + ' ' + user.last_name or ''

        def name_from_thread(thread_id):
            thread = self.client.fetchThreadInfo(thread_id)[thread_id]

            if not thread.name: # some groups have no name
                users = self.client.fetchUserInfo(*thread.participants)

                return '({})'.format(','.join([stringify_user(e) for e in users.values()]))

            return thread.name

        def last_message_is_from_user(thread_id):
            last_message = self.client.fetchThreadMessages(thread_id)

            return last_message[0].author == self.client.uid

        return [name_from_thread(e) for e in threads if not last_message_is_from_user(e)]

    def update(self):
        content = self.get_messages()

        self.detail = ','.join(content)
        self.text = str(len(content)) + ' New'

        return 'success' if not content else 'failure'
