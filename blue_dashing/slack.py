import requests
import sys
import websocket
import json
import traceback
import backoff
import time
from .base_widgets import StatusWidget, format_time
from threading import Thread
from .messages import MessagesWidget

SLACK_MAX_MESSAGES = 20
SLACK_MAX_MESSAGE_LEN = 50
SLACK_more_infos_LEN = 50


class HTTPException(Exception):
    pass

class SlackWidget(StatusWidget):
    uri = None
    token = None
    timeout = 240
    registered = False
    title = None
    ping_pattern = None
    names_cache = {}

    def __init__(self):
        MessagesWidget().register_tile(self.uri, self)

    @backoff.on_exception(backoff.constant, HTTPException, max_tries=3, interval=0.1)
    def send_request(self, uri: str, value: str, **kwargs):

        uri += '?token=%s' % self.token
        for e in kwargs:
            uri += '&%s=%s' % (e, kwargs[e])

        req = requests.get(self.uri + uri)
        req.raise_for_status()

        json = req.json()
        if not json['ok']:
            raise HttpException('%s -> %s(%i)' % (uri, req.content, req.status_code), file=sys.stderr)

        return json[value]

    def list_conversations(self):
        channels = self.send_request('/api/conversations.list', 'channels', types='public_channel,private_channel,mpim,im')

        # Strip public channels the user isn't a member of
        return [e for e in channels if e.get('is_member', True)]

    def get_last_read(self, conversation):
        return self.send_request('/api/conversations.info', 'channel', channel=conversation['id'])['last_read']

    def get_unread_messages(self, conversation):
        last_read = self.get_last_read(conversation)

        args = {'oldest': last_read} if last_read and float(last_read) != 0 else {}

        args['channel'] = conversation['id']

        return self.send_request('/api/conversations.history', 'messages', **args)


    def user_name(self, id):
        if id in self.names_cache:
            return self.names_cache[id]

        req = requests.get(self.uri + '/api/users.info?token=%s&user=%s' % (self.token, id))
        req.raise_for_status()
        json = req.json()

        if not json['ok']: # Case of bots
            return id

        self.names_cache[id] = json['user']['real_name']
        return self.names_cache[id]

    def ping_count(self, messages):

        def pinged(text):
            return text is not None and any(e in text for e in self.ping_pattern)

        return sum('text' in e and pinged(e['text']) for e in messages)


    def update(self):
        if not self.registered:
            SlackStream().register(self)
            self.registered = True

        ping = 0
        message_count = 0

        pinged_on = []
        messaged_on = []

        for e in self.list_conversations():
            messages = self.get_unread_messages(e)

            if not messages:
                continue

            name = e.get('name') or e['user']
            message_count += len(messages)
            ping_count = len(messages) if e['is_im'] else self.ping_count(messages)
            if ping_count > 0:
                ping += ping_count
                pinged_on.append(name)
            else:
                messaged_on.append(name)


        self.text = '%s pings' % ping
        self.detail = '%s messages' % message_count

        self.more_infos = ','.join(pinged_on + messaged_on)[:SLACK_more_infos_LEN]

        if ping > 0:
            return 'failure'

        return 'unstable' if message_count > 0 else 'success'



class SlackWatcher():
    def __init__(self, widget: SlackWidget):
        self.uri = widget.uri
        self.token = widget.token
        self.widget = widget


    # It appears that Slack's services are sometimes returning 500's.
    # Should we just wait & retry when this happens, or maybe throw a a specialiazed
    # exception, that wouldn't put the widget in an error state ?
    def send_request(self, uri: str, value: str, **kwargs):

        uri += '?token=%s' % self.token
        for e in kwargs:
            uri += '&%s=%s' % (e, kwargs[e])

        req = requests.get(self.uri + uri)
        req.raise_for_status()

        json = req.json()
        assert json['ok']

        return json[value]

    # Get the URI to connect to the websocket
    def get_uri(self):
        return self.send_request('/api/rtm.connect', 'url')

    # Start the websocket connection synchronusly
    def connect(self):
        self.connection = websocket.WebSocketApp(self.get_uri(),
                on_message=self.on_message,
                on_error=self.on_error,
                on_close=self.on_close)

    def propagate_message(self, author, content, ts, channel=None, bot_id=None):
        address = channel
        if not address:
            address = bot_id if bot_id else author

        self.message_received(self.widget.user_name(author),
                             format_time(round(float(ts))),
                             content,
                             '{}/messages/{}/'.format(self.uri, address),
                             self.uri
                             )

    def handle_message(self,  message):
        if 'type' not in message or message['type'] != 'message':
            return

        if not 'subtype' in message or message['subtype'] in ['me_message', 'file_share']:
            return self.propagate_message(message['user'], message['text'], message['ts'], message.get('channel'))

        if message['subtype'] == 'bot_message' and 'username' in message:
            return self.propagate_message(message['username'], message['text'], message['ts'], message.get('channel'), message.get('bot_id', 'bot'))

        if message['subtype'] in ['message_replied', 'message_deleted', 'bot_message', 'group_join', 'group_leave', 'message_changed']:
            return

        print("Can't read: %s" % message, file=sys.stderr)
        return None

    def on_message(self, _, message):
        try:
            self.handle_message(json.loads(message))

        except Exception as e:
            print(traceback.format_exc(), file=sys.stderr)
            self.error(self.uri)

    def on_error(self, _, error):
        self.error(self.uri)
        print("ERROR %s" % error)

    def on_close(self, *args):
        self.close(self.uri)

    def start(self, on_message, on_error, on_close):
        self.message_received = on_message
        self.error = on_error
        self.close = on_close

        self.connect()

        self.connection.run_forever()


class SlackStream():
    slacks = {}
    widgets = {}
    started = False
    instance = None
    widget = MessagesWidget()

    def __new__(cls):
        if cls.instance is None:
            cls.instance = object.__new__(cls)
        return cls.instance

    def watch(self, widget: SlackWatcher):
        assert widget.uri not in self.slacks

        self.slacks[widget.uri] = SlackWatcher(widget)
        thread = Thread(target = SlackWatcher.start,
                args=(self.slacks[widget.uri], self.on_message, self.on_error, self.on_close))
        thread.daemon = True
        thread.start()

        self.widget.remove_error_state(widget.uri)

    def register(self, widget: SlackWidget):
        assert not widget.uri in self.widgets

        self.widgets[widget.uri] = widget
        self.watch(widget)


    def on_message(self, author: str, time: str, content: str, url: str, widget_uri: str):
        self.widget.invalidate_tile(widget_uri)
        self.widget.add_message(author, time, content, url)

    def on_error(self, uri):
        self.widget.add_error(format_time(time.time()), 'error on: ' + uri, uri)

    def on_close(self, uri):
        print('CLOSED on: %s' % uri)

        del self.slacks[uri]
        self.watch(self.widgets[uri])
