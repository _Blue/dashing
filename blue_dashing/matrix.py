import requests
import time
import asyncio
import traceback
import logging
from threading import Thread
from nio import AsyncClient, AsyncClientConfig, RoomMessage, RoomMessageText, ReceiptEvent
from .base_widgets import StatusWidget
from .base_widgets import format_time
from .messages import MessagesWidget

class MatrixClient:
    widget = MessagesWidget()

    def __init__(self, url: str, user_id: str, token: str, device_id: str, store_path: str, key_file_path: str, key_file_passphrase: str):
        self.url = url
        self.key_file_path = key_file_path
        self.key_file_passphrase = key_file_passphrase

        config = AsyncClientConfig(store_sync_tokens=True, encryption_enabled=True)
        self.client = AsyncClient(url, user_id, config=config, store_path=store_path)
        self.client.restore_login(user_id=user_id, device_id=device_id, access_token=token)
        self.client.add_event_callback(self.on_message, RoomMessage)
        self.client.add_ephemeral_callback(self.on_receipt, ReceiptEvent)

    async def on_receipt(self, room, event):
        try:
            if any(e.user_id == self.client.user_id for e in event.receipts):
                self.widget.invalidate_tile(self.url)
        except Exception as e:
            traceback.print_exc()
            self.widget.add_error(format_time(time.time()), str(e), self.url)

    async def on_message(self, room, event):
        try:
            content = event.body if isinstance(event, RoomMessageText) else '[' + type(event).__name__ + ']'
            author = event.sender

            self.widget.add_message(author, format_time(time.time()), content, self.url)
            self.widget.invalidate_tile(self.url)
        except Exception as e:
            traceback.print_exc()
            self.widget.add_error(format_time(time.time()), str(e), self.url)

    async def run(self):
        await self.client.sync_forever(timeout=60000)

    def listen(self):
        asyncio.get_event_loop().run_until_complete(self.run())


class MatrixWidget(StatusWidget):
    token = None
    server = None
    user_id = None
    device_id = None
    store_path = None
    key_file_path = None
    key_file_passphrase = None
    timeout = 300
    client_started = False

    def __init__(self):
        MessagesWidget().register_tile(self.server, self)

    def is_unread(room: dict) -> bool:
        return MatrixWidget.mentions(room) or MatrixWidget.notifications(room)


    def get_unread_rooms(self):
        response = requests.get('{}/_matrix/client/r0/sync?set_presence=unavailable&access_token={}'.format(self.server, self.token))
        response.raise_for_status()

        body = response.json()

        return [e for e in body['rooms']['join'].values() if MatrixWidget.is_unread(e)]

    def mentions(room) -> int:
        return room['unread_notifications'].get('highlight_count', 0)

    def notifications(room) -> int:
        return room['unread_notifications'].get('notification_count', 0)

    def room_name(self, room) -> str:
        events = [e for e in room['state']['events'] if e['type'] == 'm.room.name']

        if events:
            return events[-1]['content']['name']

        # If the room isn't named, and it's a 1:1, use the other contact's name

        members = [e for e in (room['state']['events'] + room['timeline']['events'] )if e['type'] == 'm.room.member']

        contacts = [e for e in members if e['sender'] != self.user_id]
        if not contacts:
            return '[???]'
        elif len(contacts) == 1:
            return contacts[0]['content']['displayname']

        return '[' + ','.join(e['content']['displayname'] for e in contacts) + ']'


    def update(self):
        if not self.client_started:
            self.client_started = True

            def run():
                asyncio.set_event_loop(asyncio.new_event_loop())
                self.client = MatrixClient(self.server, self.user_id, self.token, self.device_id, self.store_path, self.key_file_path, self.key_file_passphrase)
                self.client.listen()

            self.thread = Thread(target=run)
            self.thread.daemon = True
            self.thread.start()

        content = self.get_unread_rooms()
        mentions = sum((MatrixWidget.mentions(e) for e in content))
        notifications = sum((MatrixWidget.notifications(e) for e in content))

        self.text = str(mentions) + ' pings'
        self.detail = str(notifications) + ' messages'
        self.more_infos = ','.join([self.room_name(e) for e in content])

        return 'success' if not content else 'failure'
