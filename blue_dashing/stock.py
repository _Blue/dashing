import requests
import backoff
import json
import time
from requests.auth import HTTPBasicAuth
from .base_widgets import GraphWidget
from datetime import datetime, timedelta

MAX_HISTORY_DAYS = 30

class StockWidget(GraphWidget):
    name = None
    token = None
    timeout = 60

    current_value = None # Holds today's value
    detail = None
    historical_values = None
    last_historical_refresh = None

    def update_stock_value(self) -> float:
        response = requests.get("https://cloud.iexapis.com/stable/stock/{}/quote?token={}".format(self.name, self.token))
        response.raise_for_status()

        content = response.json()
        self.current_value = content['latestPrice']


        state = content['calculationPrice']
        variation = content['change'] if state == 'tops' else content['extendedChange']

        if variation:
            self.detail = '{}{}$'.format('+' if variation >= 0 else '', str(variation)[:5])
        else:
            self.detail = ''

    def get_historical_values(self):
        response = requests.get('https://cloud.iexapis.com/stable/stock/{}/chart/1m?token={}'.format(self.name, self.token))
        assert response.status_code == 200

        return [e["close"] for e in response.json() if "close" in e and e["close"]]

    def update(self):
        self.update_stock_value()

        if not self.last_historical_refresh or self.last_historical_refresh + timedelta(hours=6) < datetime.now():
            self.historical_values = self.get_historical_values()
            self.last_historical_refresh = datetime.now()

    def get_value(self):
        self.refresh()

        return self.status + ":" + self.detail if self.status else self.current_value

    def get_detail(self):
        return self.detail

    def get_data(self):
        self.refresh()

        def make_row(position, value) -> dict:
            return {'x': position, 'y': value}

        return [make_row(e[0], e[1]) for e in enumerate(self.historical_values + [self.current_value])]

    def get_more_info(self):
        return self.get_updated_at()
