from .facebook import *
from .slack import *
from .crypto_currencies import *
from .service_widgets import *


# Create your widgets in this file

widgets = [] # See README for examples
