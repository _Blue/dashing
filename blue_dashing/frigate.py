import requests
import socket

from datetime import datetime
from humanize import naturaltime
from .base_widgets import InformedStatusWidget

class FrigateWidget(InformedStatusWidget):
    url = None
    request_timeout = 15

    def update(self):
        self.extra = []
        self.detail = ''
        self.text = ''

        response = requests.get(self.url, timeout=30)
        response.raise_for_status()


        def format_event(event: dict):
            zones = ','.join(event['zones']) +', ' if event.get('zones', None) else ''

            diff = naturaltime(datetime.now() - datetime.fromtimestamp(event['start_time']))

            prefix = 'ONGOING: ' if event.get('end_time', None) else ''

            return {'label': event.get('label', '?'), 'value': f'{zones}{diff}({event["camera"]})'}

        events = response.json()
        self.extra = [format_event(e) for e in events]

        # Show the widget in red if at least one event is in progress
        return 'success' if not any(e.get('end_time', None) is None for e in events) else 'failure'
