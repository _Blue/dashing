import requests
import backoff
import json
from datetime import date, timedelta
from requests.auth import HTTPBasicAuth
from .base_widgets import RefreshingWidget, GraphWidget
from dashing.widgets import NumberWidget


# Not very elegant, but works for now
latest_eth_value = None


class CryptoCurrency(GraphWidget):
    symbol = None
    secondary_symbol = None
    token = None
    timeout = 3600

    current_value = None # Holds today's value
    detail = None
    historical_values = []

    def get_historical_values(self):
        response = requests.get('https://cloud.iexapis.com/stable/stock/{}/chart/1m?token={}'.format(self.name, self.token))
        assert response.status_code == 200

        return [e["close"] for e in response.json() if "close" in e and e["close"]]

    def get_symbol_rate(self, symbol: str) -> float:
        headers = {'X-CoinAPI-Key': self.token}
        response = requests.get(f'https://rest.coinapi.io/v1/exchangerate/{symbol}/USD', headers=headers)
        response.raise_for_status()

        return response.json()['rate']

    def update(self):
        today = date.today()
        last_month = today - timedelta(days=31)
        headers = {'X-CoinAPI-Key': self.token}

        response = requests.get(f'https://rest.coinapi.io/v1/exchangerate/{self.symbol}/USD/history?period_id=1DAY&time_start={last_month.isoformat()}&time_end={today.isoformat()}', headers=headers)
        response.raise_for_status()

        self.historical_values = [e['rate_close'] for e in response.json()]

        self.current_value = self.get_symbol_rate(self.symbol)
        global latest_eth_value
        latest_eth_value = self.current_value

        self.detail = f'{self.secondary_symbol}: {self.get_symbol_rate(self.secondary_symbol):.2f}$'

    def get_value(self):
        return f'{self.current_value:.2f}$'

    def get_detail(self):
        return self.detail

    def get_data(self):
        def make_row(position, value) -> dict:
            return {'x': position, 'y': value}

        return [make_row(e[0], e[1]) for e in enumerate(self.historical_values + [self.current_value])]

    def get_more_info(self):
        return self.get_updated_at()

class Mining(RefreshingWidget, NumberWidget):
    address = None
    timeout = 120

    @backoff.on_exception(backoff.constant, (AssertionError, json.decoder.JSONDecodeError), max_tries=5, interval=0.5)
    def update(self):
        content = requests.get('https://api.nanopool.org/v1/eth/user/' + self.address)

        assert content.status_code

        json = content.json()
        assert(json['status'])

        self.text = json['data']['avgHashrate']['h1']
        self.detail = 'Pending: %s' % json['data']['balance']
        self.more_infos = 'Average: %s' % json['data']['avgHashrate']['h24']


class EthereumBalance(RefreshingWidget, NumberWidget):
    token = None
    account = None
    timeout = 3000

    def update(self):
        uri = 'https://api.etherscan.io/api?module=account&action=balance&address={}&tag=latest&apikey={}'.format(self.account, self.token)

        result = requests.get(uri)
        result.raise_for_status()

        balance = float(result.json()['result']) / 10 ** 18

        self.text = str(round(balance, 6))
        self.detail = '{}$'.format(round(balance * latest_eth_value, 2)) if latest_eth_value else '[ETH price not available]'
        self.more_infos = '1 ETH = {}$'.format(latest_eth_value)
