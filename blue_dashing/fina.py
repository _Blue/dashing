from .base_widgets import StatusWidget, format_time
import requests
import json

states = {
            "Opened": "success",
            "Locking": "unstable",
            "Locked": "success",
            "Triggered": "unstable",
            "Alerted": "failure",
         }

class FinaChecker(StatusWidget):
    uri = None
    timeout = 60

    def update(self):
        response = requests.get(self.uri)

        if response.status_code != 200:
            raise RuntimeError("HTTP query failed with: %i" % response.status_code)

        content = response.json()

        self.detail = "Since: "  + format_time(content["ts"])

        self.text = content["state"]
        return states[content["state"]]
