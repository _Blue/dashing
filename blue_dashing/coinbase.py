import requests
from coinbase.wallet.client import Client
from .base_widgets import InformedStatusWidget

class CoinbaseWidget(InformedStatusWidget):
    api_key = None
    api_secret = None
    client = None

    def update(self):
        if not self.client:
            self.client = Client(self.api_key, self.api_secret)

        accounts = self.client.get_accounts()
        accounts = [e for e in accounts.data if float(e['balance']['amount']) != 0]
        self.extra = []
        self.detail = ''

        for e in accounts:
            value = '{}$ ({})'.format(e['native_balance']['amount'], e['balance']['amount'])

            self.extra.append({'label': e['balance']['currency'], 'value': value})

        total = sum(float(e['native_balance']['amount']) for e in accounts)
        self.extra.append({'label': 'Total', 'value': str(round(total, 2)) + '$'})

        assert(all(e['native_balance']['currency'] == 'USD' for e in accounts))

        return 'success'
