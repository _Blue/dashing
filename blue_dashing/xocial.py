import requests
import websockets
import asyncio
import time
import json
import traceback
from threading import Thread
from .base_widgets import StatusWidget, format_time
from .messages import MessagesWidget


class XocialWatcher():
    def __init__(self, endpoint):
        self.uri = endpoint
        self.widget = MessagesWidget()

        self.processed_messages = set()

    async def run_impl(self):
        async with websockets.connect(self.uri.replace('http', 'ws') + "/ws/update") as connection:
            await connection.send('{"action": "subscribe", "types": ["Message"]}')

            result = await connection.recv()

            assert json.loads(result)['result'] == 'OK'

            while True:
                self.on_message(await connection.recv())

    def run(self):
        look = asyncio.new_event_loop()

        print('XocialWatcher starting')
        while True:
            try:
                look.run_until_complete(self.run_impl())
            except Exception as e:
                print('Xocial restarting: ' + str(e))
                print(traceback.format_exc())

            print('Restarting XocialWatcher')
            time.sleep(60)


    def on_message(self, payload):
        try:
            self.on_message_impl(payload)
        except Exception as e:
            details = traceback.format_exc()
            self.widget.add_error(format_time(time.time()), 'error in xocial message processing: ' + details, self.uri)
            print(details)

    def on_error(self, error):
        self.widget.add_error(format_time(time.time()), 'error in xocial message processing: ' + str(error), self.uri)

    def on_close(self):
       self.widget.add_error(format_time(time.time()), 'Closed in xocial message processing', self.uri)


    def on_message_impl(self, payload):
        message = json.loads(payload)

        if message['id'] in self.processed_messages:
            return

        self.processed_messages.add(message['id'])

        if message['text'] or not message['attachments']:
            self.widget.add_message(message['author']['display_name'], message['timestamp'], message['text'], self.uri + '/../xocial/message/' + str(message['id']) )

        for e in message['attachments']:
            self.widget.add_message(message['author']['display_name'], message['timestamp'], e['type'].split('/')[-1], self.uri + '/../xocial/message/' + str(message['id']) )

class XocialWidget(StatusWidget):
    timeout = 120
    endpoint = None
    started = False

    def __init__(self):
        MessagesWidget().register_tile(self.endpoint, self)

    def get_conversations(self):
        response = requests.get('{}/conversation/unread'.format(self.endpoint))
        response.raise_for_status()

        return response.json()

    def start(self):
        watcher = XocialWatcher(self.endpoint)

        thread = Thread(target=XocialWatcher.run, args=(watcher,))
        thread.start()

    def update(self):
        if not self.started:
            self.start()
            self.started = True

        content = self.get_conversations()

        self.detail = ','.join([e['display_name'] for e in content])
        self.text = str(len(content)) + ' New'
        self.more_infos = str(sum(e['unread_count'] for e in content)) + ' messages'

        return 'success' if not content else 'failure'
