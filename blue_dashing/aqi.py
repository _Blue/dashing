import requests
from .base_widgets import StatusWidget

class AQIWidget(StatusWidget):
    city = None
    state = None
    country = None
    token = None
    timeout = 500

    def update(self):
        uri = 'https://api.airvisual.com/v2/city?city={}&state={}&country={}&key={}'.format(self.city, self.state, self.country, self.token)
        response = requests.get(uri)
        response.raise_for_status()

        pollution = response.json()['data']['current']['pollution']
        aqi = pollution['aqius']
        self.text = '{}'.format(aqi)
        self.detail = 'AQICN: {}'.format(pollution['aqicn'])
        self.more_infos = 'Main pollutant: {}'.format(pollution['mainus'])

        if aqi < 50:
            return 'success'
        elif aqi < 150:
            return 'unstable'
        else:
            return 'failure'

class AQICNWidget(StatusWidget):
    city = None
    token = None
    timeout = 500

    def update(self):
        uri = 'https://api.waqi.info/feed/{}/?token={}'.format(self.city, self.token)
        response = requests.get(uri)
        response.raise_for_status()

        data = response.json()

        if data['status'] != 'ok':
            raise RuntimeError('Error status: ' + data['status'])

        aqi = data['data']['aqi']
        self.text = '{}'.format(aqi)
        self.more_infos = 'Main pollutant: {}'.format(data['data']['dominentpol'])

        if aqi < 50:
            return 'success'
        elif aqi < 150:
            return 'unstable'
        else:
            return 'failure'

