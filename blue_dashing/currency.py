import requests
import backoff
import json
import time
from .base_widgets import RefreshingWidget
from dashing.widgets import GraphWidget
from datetime import datetime, timedelta

MAX_HISTORY_DAYS = 30
HISTORY_URI = 'https://api.apilayer.com/exchangerates_data/timeseries?start_date={}&end_date={}&base={}&symbols={}'
LATEST_URI = 'https://api.apilayer.com/exchangerates_data/latest?base={}&symbols={}'

class CurrencyWidget(RefreshingWidget, GraphWidget):
    base = None
    target = None
    status = None
    token = None
    timeout = 12 * 60 * 60 # 12 hours

    current_value = None # Holds today's value
    historical_values = []

    def get_latest_value(self) -> float:
        headers = {'apiKey': self.token}
        response = requests.get(LATEST_URI.format(self.base, self.target), headers=headers)
        response.raise_for_status()

        assert response.status_code == 200

        return response.json()["rates"][self.target]

    def get_historical_values(self):
        end = datetime.today()
        start = end - timedelta(days=MAX_HISTORY_DAYS)

        def date_str(date):
            return date.strftime('%Y-%m-%d')

        headers = {'apiKey': self.token}
        response = requests.get(HISTORY_URI.format(date_str(start), date_str(end), self.base, self.target), headers=headers)
        response.raise_for_status()

        content = response.json()["rates"]

        dates = [end - timedelta(days=e) for e in range(0, MAX_HISTORY_DAYS)]

        values = []
        for e in reversed(dates):
            key = date_str(e)

            if key in content:
                values.append(content[key][self.target])
            elif values:
                values.append(values[-1])

        return values

    def update(self):
        self.current_value = float(round(self.get_latest_value(), 6))
        self.historical_values = self.get_historical_values()

    def get_value(self):
        self.refresh()

        return self.status + ":" + self.detail if self.status else self.current_value

    def get_data(self):
        self.refresh()

        def make_row(position, value) -> dict:
            return {'x': position, 'y': value}

        return [make_row(e[0], e[1]) for e in enumerate(self.historical_values + [self.current_value])]

    def get_more_info(self):
        return self.get_updated_at()
