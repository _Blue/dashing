# -*- encoding: utf-8 -*-
from dashing.widgets import ListWidget
from django.utils import timezone
import icalendar
import requests
import time
import pytz
from tzlocal import get_localzone
from datetime import datetime, date, timedelta, date
from .base_widgets import RefreshingWidget, TIME_FORMAT
from .settings import TIME_ZONE
from django.utils.timezone import activate
from dateutil.rrule import rrulestr

MAX_EVENTS = 8
MAX_EVENT_DESCRIPTION = 100
activate(TIME_ZONE)

class Calendar(ListWidget, RefreshingWidget):
    instance = None
    uri = None
    auth = None
    max_events = None
    timeout = 500

    def __new__(cls):
        if cls.instance is None:
            cls.instance = object.__new__(cls)
        return cls.instance

    def localize(ts) -> datetime:
        if not isinstance(ts, datetime): # In case of date, convert to datetime (set 00:00)
            assert isinstance(ts, date)
            return timezone.get_current_timezone().localize(datetime.combine(ts, datetime.min.time()))
        else:
            return ts.astimezone(timezone.get_current_timezone())

    def get_events(self) -> list:
        response = requests.get(self.uri, auth=self.auth)
        response.raise_for_status()

        return [e for e in icalendar.Calendar.from_ical(response.text).subcomponents if e.has_key('summary')]


    def get_event_begin(event) -> datetime:
        try:
            begin = Calendar.localize(event['dtstart'].dt)

            # If event has no repeat rule, just return start time
            if not 'RRULE' in event:
                return begin

            end = Calendar.localize(event['until'].dt) if 'until' in event else None
            now = Calendar.localize(datetime.utcnow().replace(tzinfo=pytz.utc))

            if end is not None and now > end: # If event is expired
                return end

            rule = rrulestr(event['rrule'].to_ical().decode(), dtstart=begin)

            next_occurence = rule.after(now)

            return Calendar.localize(next_occurence) if next_occurence else None
        except Exception as e:
            raise RuntimeError(f'Failed to process event {event["uid"]}') from e


    def make_row(self, event, begin) -> dict:
        description = event['location'].title() if 'location' in event else ''

        if 'description' in event:
            description += ', ' if description else '' + event['description'].title()

        return {
                'url': '',
                'author': begin.strftime(TIME_FORMAT),
                'sha': event['summary'].title(),
                'message': description[:MAX_EVENT_DESCRIPTION:]
                }

    def update(self):
        self.detail = None
        now = timezone.now()

        events = [e for e in self.get_events() if 'dtstart' in e] # Some tasks don't have start dates

        # Compute start time for each event
        events = [(Calendar.get_event_begin(e), e) for e in events]

        # Remove events that are expired (if start time is None or in the past)
        events = [e for e in events if e[0] and e[0] >= now]

        # Sort by time
        events.sort(key= lambda e: e[0])

        # Take up to MAX_EVENTS entries
        events = [e for e in events][:MAX_EVENTS]

        return [self.make_row(e[1], e[0]) for e in events]

    def get_data(self):

        self.refresh()

        if self.detail:
            return [{'url': '', 'author': 'ERROR', 'sha': 'ERROR', 'message': self.detail}]

        return self.status

