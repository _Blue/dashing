import time
import requests
import json
import traceback
import requests.auth
import datetime
import vobject
import signal
from threading import Thread
from .base_widgets import StatusWidget
from .base_widgets import format_time
from .messages import MessagesWidget
from subprocess import Popen, PIPE
from multiprocessing import Lock



class Message:
    def __init__(self, content: dict, sender: str, sender_uid: str, group_id: str, resolved_author: str):
        self.content = content
        self.sender = sender
        self.sender_uid = sender_uid
        self.author_str = resolved_author
        self.group_id = group_id
    @property
    def text(self):
        text = self.content.get('text') or self.content.get('message', '')

        attachments = ','.join([f'Attachment({e["contentType"]})' for e in self.content.get('attachments', [])])
        if attachments:
            if not text:
                return attachments
            else:
                return text + ' ' + attachments
        else:
            return text

    @property
    def author(self):
        return self.sender

    @property
    def author_uuid(self):
        return self.sender_uid

    @property
    def timestamp(self):
        return self.content['timestamp']

    @property
    def group(self):
        return self.group_id

    @property
    def resolved_author(self):
        return self.author_str

class SignalClient:
    widget = MessagesWidget()

    def __init__(self, resolve_contact, signal_cli_path, on_error):
        self.resolve_contact = resolve_contact
        self.unread_messages = []
        self.read_ts = {}
        self.lock = Lock()
        self.signal_cli_path = signal_cli_path
        self.on_error = on_error

    def on_message(self, content: dict, sender: str, sender_uid: str, backup_name: str):
        if not content.get('text', None) and not content.get('message') and not content.get('attachments'): # Reactions have no text
            return

        author = self.resolve_contact(sender) if sender else backup_name
        if backup_name and not author:
            author = backup_name

        message = Message(content, sender, sender_uid, content.get('groupInfo', {}).get('groupId'), author)

        self.widget.add_message(author or '[Me]', format_time(time.time()), message.text, 'signal')
        self.widget.invalidate_tile('signal')

        if author:
            with self.lock:
                self.unread_messages.append(message)

    def update_read_ts(self, sender_uid, ts):
        with self.lock:
            self.unread_messages = [e for e in self.unread_messages if e.author_uuid != sender_uid or e.timestamp > ts]

    def get_unread(self):
        with self.lock:
            return self.unread_messages

    def on_event(self, content: dict):
        try:
            if 'dataMessage' in content:
                return self.on_message(content['dataMessage'], content.get('sourceNumber', None), content.get('sourceUuid', None), content.get('sourceName', None))
            elif 'syncMessage' in content:
                if 'sentMessage' in content['syncMessage']:
                    return self.on_message(content['syncMessage']['sentMessage'], None, None, None)

                read_messages = content['syncMessage'].get('readMessages', [])
                if not read_messages:
                    return

                for e in read_messages:
                    self.update_read_ts(e['senderUuid'], e['timestamp'])
                self.widget.invalidate_tile('signal')
        except Exception as e:
            traceback.print_exc()
            self.widget.add_error(format_time(time.time()), str(e), 'signal')


    def run(self):
        # signal-cli has a tendency to stay even after its parent dies. Use prctl to make sure it's killed when dashing stops (very very hacky)
        try:
            process = Popen(['/usr/bin/setpriv', '--pdeathsig', 'SIGKILL', self.signal_cli_path, '-o', 'json', 'receive', '--ignore-attachments', '-t', '-1'], stdout=PIPE)
            line = process.stdout.readline()

            while line:
                self.on_event(json.loads(line)['envelope'])
                line = process.stdout.readline()

            raise RuntimeError('Signal-cli has exited')
        except Exception as e:
            traceback.print_exc()
            self.widget.add_error(format_time(time.time()), str(e), 'signal')
            self.on_error(e)


class SignalWidget(StatusWidget):
    timeout = 300
    resolve_contact = None
    client = None
    signal_cli_path = None
    error = None

    def __init__(self):
        MessagesWidget().register_tile('signal', self)

    def on_error(self, error):
        self.error = error

    def update(self):
        if not self.client:
            self.client = SignalClient(self.resolve_contact, self.signal_cli_path, self.on_error)

            self.thread = Thread(target=self.client.run)
            self.thread.daemon = True
            self.thread.start()

        if self.error is not None:
            raise self.error

        messages = self.client.get_unread()
        mentions = 0 # TODO
        notifications = len(messages)

        self.text = str(mentions) + ' pings'
        self.detail = str(notifications) + ' messages'
        self.more_infos = ','.join(set([e.resolved_author or 'Me' for e in messages]))

        return 'success' if not messages else 'failure'
