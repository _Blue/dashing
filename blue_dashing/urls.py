# -*- coding: utf-8 -*-
import re
from django.conf.urls import include, url
from django.views.generic.base import RedirectView
from .widgets import widgets
from dashing.views import Dashboard

widgets_urls = [url(r'^$', Dashboard.as_view(), name='dashboard')]

for e in widgets:
    class_name = e['class'].__name__ + '_' + e['title']

    args = e['args'] if 'args' in e else {}

    widget_type = type(class_name, (e['class'],), args)
    setattr(widget_type, 'title', e['title'])

    widgets_urls.append(url(r'^widgets/{}'.format(e['title']),
                    widget_type.as_view(),
                    name='widget_{}'.format(e['title'])))


urlpatterns = [
    url(r'^dashboard/', include(widgets_urls), name='dashboard'),
    url(r'^$', RedirectView.as_view(url='dashboard/'), name='index')
]


