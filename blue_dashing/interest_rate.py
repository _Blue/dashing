import requests
import time
from .base_widgets import GraphWidget
from datetime import datetime, timedelta, date
from dateutil.relativedelta import relativedelta

class InterestRateWidget(GraphWidget):
    timeout = 3600
    historical_range = timedelta(days = 365 * 5)

    text = ''
    detail = ''
    historical_values = []
    more_infos = ''


    def parse_entry(self, entry: str):
        values = entry.split(',')

        if len(values) != 2:
            raise RuntimeError(f'Unexpected entry: {entry}')

        return datetime.strptime(values[0], '%Y-%m-%d').date(), float(values[1])

    def update(self):
        date = (datetime.now()- self.historical_range).strftime('%Y-%m-%d')

        response = requests.get(f'https://fred.stlouisfed.org/graph/fredgraph.csv?id=FEDFUNDS&cosd={date}')
        response.raise_for_status()

        entries = [self.parse_entry(e) for e in response.text.replace('\r', '').split('\n')[1:] if e]
        entries = {date: value for date, value in entries}

        # Entries are monthly, but some might be skipped, so start from the oldest one and iterate to fill the gaps
        current = min(entries)
        end = max(entries)
        historical_values = []

        while current <= end:
            if current in entries:
                historical_values.append(entries[current])
            else:
                historical_values.append(historical_values[-1])

            current = current + relativedelta(months=1)

        self.historical_values = historical_values
        self.text = f'{historical_values[-1]}%'


        diff = self.historical_values[-1] - self.historical_values[-2]
        self.detail = f'{"+" if diff >= 0 else "-"}{diff:.2f}%'

    def get_data(self):
        def make_row(position, value) -> dict:
            return {'x': position, 'y': value}

        return [make_row(e[0], e[1]) for e in enumerate(self.historical_values)]

