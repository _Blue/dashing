import requests
import html
import json
from bs4 import BeautifulSoup
from .base_widgets import InformedStatusWidget

API_ENDPOINT='https://api.entur.io/journey-planner/v2/graphql'

def get_next_departures(stop_id: int) -> list:
    query = '''{
    stopPlace(id: "NSR:StopPlace:%s") {
      id
      name
      estimatedCalls(timeRange: 721000, numberOfDepartures: 10) { 
        expectedDepartureTime
        destinationDisplay {
          frontText
        }
        serviceJourney {
          journeyPattern {
            line {
              id
            }
          }
        }
      }
    }
}''' % stop_id

    post_data = {'query': query, 'variables': None}

    headers = {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
              }
    content = requests.post(API_ENDPOINT, headers=headers, data=json.dumps(post_data))

    if not content.status_code == 200:
        raise RuntimeError("Entur API endpoint returned: %i" % content.status_code)

    return make_rows(content.json()['data']['stopPlace']['estimatedCalls'])

def extract_time(timestamp: str) -> str: # Fairly ugly, but let's avoid dealing with timezones here
    return timestamp.split('T')[1].split('+')[0]

def extract_line(entry: dict) -> str:
    id = entry['serviceJourney']['journeyPattern']['line']['id'].split(':')[-1]
    direction = entry['destinationDisplay']['frontText']

    return id + ' - ' + direction

def make_rows(content: str) -> list:
    return [(extract_line(e), extract_time(e['expectedDepartureTime'])) for e in content]

# Widget that displays the next transports (bus or trains)
# leaving a specific stop in the city of Oslo, Norway
class OsloTransportWidget(InformedStatusWidget):
    stop_id = None
    timeout = 60

    def update(self):
        self.detail = '' # Reset any error state
        rows = []

        for entry in get_next_departures(self.stop_id):
            if not any(e['label'] == entry[0] for e in rows): # Make each entry unique
                rows.append({'label': entry[0], 'value': entry[1]})

        self.extra = rows

        return 'success'
