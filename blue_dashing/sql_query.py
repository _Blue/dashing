from .base_widgets import RefreshingWidget
from dashing.widgets import GraphWidget
from datetime import datetime, timedelta
import psycopg2


class SqlQueryWidget(RefreshingWidget, GraphWidget):
    status = None
    database = None
    sql_query = None
    timeout = 12 * 60 * 60 # 12 hours
    current_value = None # Holds today's value

    def query(self, database):
        with database.cursor() as cursor:
            cursor.execute(self.sql_query)

            return [e[0] for e in cursor.fetchall()]

    def update(self):
        with psycopg2.connect(**self.database) as db:
            self.values = self.query(db)

        self.current_value = self.values[-1]

    def get_value(self):
        self.refresh()

        return self.status + ":" + self.detail if self.status else self.current_value

    def get_data(self):
        self.refresh()

        def make_row(position, value) -> dict:
            return {'x': position, 'y': value}
        return [make_row(e[0], e[1]) for e in enumerate(self.values)]

    def get_more_info(self):
        return self.get_updated_at()
